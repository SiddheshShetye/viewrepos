package com.siddroid.viewrepos.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.pnikosis.materialishprogress.ProgressWheel

import com.siddroid.viewrepos.R
import com.siddroid.viewrepos.adapter.*
import com.siddroid.viewrepos.presenter.RepoContract
import com.siddroid.viewrepos.presenter.RepoPresenter
import com.siddroid.viewrepos.wsmodel.Branch
import com.siddroid.viewrepos.wsmodel.Contributor
import com.siddroid.viewrepos.wsmodel.Repository
import java.util.ArrayList

class RepositoryListFragment : Fragment(), RepoContract.View {

    @JvmField val LIST_TYPE: String = "mListType"
    @JvmField val OWNER_NAME: String = "mOwnerName"
    @JvmField val REPO_NAME: String = "repo_name"

    private val TAG: String = "RepositoryListFragment"

    private lateinit var mAdapter: ViewRepoGenericAdapter<Repository, RepoListViewHolder>
    private lateinit var mContributorAdapter: ViewRepoGenericAdapter<Contributor, ContributorListViewHolder>
    private lateinit var mLanguageAdapter: ViewRepoGenericAdapter<String, LanguagesListViewHolder>
    private lateinit var mBranchAdapter: ViewRepoGenericAdapter<Branch, BranchListViewHolder>
    private lateinit var mRepoList: ArrayList<Repository>
    private lateinit var mContributorList: ArrayList<Contributor>
    private lateinit var mLanguageList: ArrayList<String>
    private lateinit var mBranchList: ArrayList<Branch>
    private lateinit var mPresenter: RepoPresenter
    private lateinit var rvLists: RecyclerView
    private lateinit var prgLoading: ProgressWheel
    private var isDataLoading: Boolean = false
    private var reachedLastPage: Boolean = false
    private lateinit var toolbar: Toolbar
    private var mListType: Int = -1
    private var mOwnerName: String? = ""
    private var mRepoName: String? = ""
    private var isDataLoaded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = arguments
        mListType = data.getInt(LIST_TYPE)
        mOwnerName = data.getString(OWNER_NAME)
        mRepoName = data.getString(REPO_NAME)
        when(mListType){
            RepoPresenter.REPO_LIST -> mRepoList = ArrayList()
            RepoPresenter.CONTRIBUTOR_LIST -> mContributorList = ArrayList()
            RepoPresenter.LANGUAGES_LIST -> mLanguageList = ArrayList()
            RepoPresenter.BRANCHES_LIST -> mBranchList = ArrayList()
            else -> Log.e(TAG,"wrong mRepoList type passed")
        }
        mPresenter = RepoPresenter(this)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_repository_list, container, false)
        rvLists = view.findViewById(R.id.rvList) as RecyclerView
        prgLoading = view.findViewById(R.id.progress) as ProgressWheel
        toolbar = view.findViewById(R.id.toolbar) as Toolbar

            when(mListType){
                RepoPresenter.REPO_LIST -> {
                    initRepoList()
                    if (!isDataLoaded) {
                        isDataLoaded = true
                        mPresenter.loadAllRepositories("0")
                    }
                }
                RepoPresenter.CONTRIBUTOR_LIST -> {
                    initContributorList()
                    mPresenter.loadAllContributors(mOwnerName, mRepoName, "1")
                }
                RepoPresenter.LANGUAGES_LIST -> {
                    initLanguagesList()
                    mPresenter.loadAllLanguages(mOwnerName, mRepoName,"1")
                }
                RepoPresenter.BRANCHES_LIST -> {
                    initBranchesList()
                    mPresenter.loadAllBranches(mOwnerName, mRepoName,"1")
                }
                else -> Log.e(TAG,"wrong mRepoList type passed")
            }
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { activity.onBackPressed() }
        var toolbarTitle = ""
        val actionbar = (activity as AppCompatActivity).supportActionBar
        when(mListType) {
            RepoPresenter.REPO_LIST -> {
                toolbarTitle = getString(R.string.repositories)
                actionbar!!.setDisplayHomeAsUpEnabled(false)
                actionbar.setHomeButtonEnabled(false)
            }
            RepoPresenter.CONTRIBUTOR_LIST -> {
                toolbarTitle = getString(R.string.contributor_title, mRepoName)
                actionbar!!.setDisplayHomeAsUpEnabled(true)
                actionbar.setHomeButtonEnabled(true)
            }
            RepoPresenter.LANGUAGES_LIST -> {
                toolbarTitle = getString(R.string.languages_title, mRepoName)
                actionbar!!.setDisplayHomeAsUpEnabled(true)
                actionbar.setHomeButtonEnabled(true)
            }
            RepoPresenter.BRANCHES_LIST -> {
                toolbarTitle = getString(R.string.branches_title, mRepoName)
                actionbar!!.setDisplayHomeAsUpEnabled(true)
                actionbar.setHomeButtonEnabled(true)
            }
            else -> Log.e(TAG,"wrong mRepoList type passed")
        }
        actionbar!!.title = toolbarTitle
    }

    override fun onStop() {
        super.onStop()
        mPresenter.cancelAllPendingRequest()
    }

    override fun showRepoList(list: List<Repository>) {
        this.mRepoList.addAll(list)
        mAdapter.notifyDataSetChanged()
    }

    override fun showContributorList(list: List<Contributor>) {
        this.mContributorList.addAll(list)
        mContributorAdapter.notifyDataSetChanged()
    }

    override fun showError() {
        Toast.makeText(activity,getString(R.string.error),Toast.LENGTH_LONG).show()
    }

    override fun navigateToNextScreen(listToLoad: Int, repo: Repository) {
        val contributorListFragment =  RepositoryListFragment()
        val data = Bundle()
        data.putString(REPO_NAME,repo.name)
        data.putString(OWNER_NAME,repo.owner.login)
        data.putInt(LIST_TYPE,listToLoad)
        contributorListFragment.arguments = data
        activity.supportFragmentManager.beginTransaction().replace(R.id.frmContainer, contributorListFragment).addToBackStack("").commit()
    }

    override fun initRepoList(){
        mAdapter = ViewRepoGenericAdapter<Repository, RepoListViewHolder>(RepoListDataBinder(mPresenter, mRepoList))
        initList()
        rvLists.adapter = mAdapter
        rvLists.addOnScrollListener(recyclerViewOnScrollListener)
    }



    override fun initContributorList(){
        mContributorAdapter = ViewRepoGenericAdapter<Contributor, ContributorListViewHolder>(ContributorListDataBinder(mContributorList))
        initList()
        rvLists.adapter = mContributorAdapter
        rvLists.addOnScrollListener(recyclerViewOnScrollListener)
    }

    override fun initLanguagesList() {
        mLanguageAdapter = ViewRepoGenericAdapter<String, LanguagesListViewHolder>(LanguageListDataBinder(mLanguageList))
        initList()
        rvLists.adapter = mLanguageAdapter
        rvLists.addOnScrollListener(recyclerViewOnScrollListener)
    }

    override fun initBranchesList() {
        mBranchAdapter = ViewRepoGenericAdapter<Branch, BranchListViewHolder>(BranchListDataBinder(mBranchList))
        initList()
        rvLists.adapter = mBranchAdapter
        rvLists.addOnScrollListener(recyclerViewOnScrollListener)
    }

    private fun initList(){
        val manager = LinearLayoutManager(activity)
        rvLists.layoutManager = manager
    }

    override fun showLanguagesList(list: Map<String, String>) {
        this.mLanguageList.addAll(LinkedHashSet<String>(list.keys))
        mLanguageAdapter.notifyDataSetChanged()
    }

    override fun showBranchesList(list: List<Branch>) {
        this.mBranchList.addAll(list)
        mBranchAdapter.notifyDataSetChanged()
    }

    override fun hideLoading() {
        isDataLoading = false
        prgLoading.visibility = View.INVISIBLE
    }

    override fun showLoading() {
        isDataLoading = true
        prgLoading.visibility = View.VISIBLE
    }

    override fun reachedLastPage() {
        reachedLastPage = true
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = recyclerView!!.layoutManager.childCount
            val totalItemCount = recyclerView.layoutManager.itemCount
            val firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

            if (!isDataLoading && firstVisibleItemPosition + visibleItemCount + visibleItemCount >= totalItemCount && !reachedLastPage) {
                val listToLoad = mListType
                when(listToLoad){
                    RepoPresenter.REPO_LIST -> mPresenter.loadNextRepoPage()
                    RepoPresenter.CONTRIBUTOR_LIST -> mPresenter.loadNextContributorPage(mOwnerName, mRepoName)
                    RepoPresenter.LANGUAGES_LIST -> mPresenter.loadNextLanguagePage(mOwnerName, mRepoName)
                    RepoPresenter.BRANCHES_LIST -> mPresenter.loadNextBranchPage(mOwnerName, mRepoName)
                    else -> Log.e(TAG,"wrong mRepoList type passed")
                }

            }
        }
    }

}
