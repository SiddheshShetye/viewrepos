package com.siddroid.viewrepos.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.wsmodel.Branch;

/**
 * The type Branch list view holder.
 * @author Siddhesh
 */
public class BranchListViewHolder extends RecyclerView.ViewHolder {

    private TextView tvName;

    /**
     * Instantiates a new Branch list view holder.
     *
     * @param itemView the item view
     */
    public BranchListViewHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.lblName);
    }

    /**
     * Bind.
     *
     * @param branch the branch
     */
    public void bind(Branch branch){
        tvName.setText(branch.getName());
    }
}
