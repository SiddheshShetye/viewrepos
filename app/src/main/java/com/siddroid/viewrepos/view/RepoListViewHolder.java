package com.siddroid.viewrepos.view;

import android.annotation.TargetApi;
import android.content.Context;
//import android.support.v7.widget.PopupMenu;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.presenter.RepoPresenter;
import com.siddroid.viewrepos.wsmodel.Repository;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

/**
 * The type Repo list view holder.
 * @author Siddhesh
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class RepoListViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener, android.support.v7.widget.PopupMenu.OnMenuItemClickListener{

    private final RepoPresenter mPresenter;
    private ImageView imvPic;
    private TextView tvName;
    private Repository mRepo;
    private WeakReference<Context> mContextRef;

    /**
     * Instantiates a new Repo list view holder.
     *
     * @param itemView  the item view
     * @param mPresenter the mPresenter
     */
    public RepoListViewHolder(View itemView, RepoPresenter mPresenter) {
        super(itemView);
        mContextRef = new WeakReference<>(itemView.getContext());
        tvName = (TextView) itemView.findViewById(R.id.lblName);
        imvPic = (ImageView) itemView.findViewById(R.id.imvPic);
        ImageButton ibtnMenu = (ImageButton) itemView.findViewById(R.id.ibtnMore);
        this.mPresenter = mPresenter;
        ibtnMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                showPopupMenu(view);
            }
        });
    }

    /**
     * Bind.
     *
     * @param repo the mRepo
     */
    public void bind(Repository repo){
        this.mRepo = repo;
        tvName.setText(repo.getName());
        Picasso.with(mContextRef.get()).load(repo.getOwner().getAvatar_url()).into(imvPic,new Callback() {
            @Override
            public void onSuccess() {
                Bitmap imageBitmap = ((BitmapDrawable) imvPic.getDrawable()).getBitmap();
                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContextRef.get().getResources(), imageBitmap);
                imageDrawable.setCircular(true);
                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                imvPic.setImageDrawable(imageDrawable);
            }
            @Override
            public void onError() {
                imvPic.setImageResource(R.drawable.ic_launcher_background);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void showPopupMenu(View view) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB){
            android.support.v7.widget.PopupMenu popup = new android.support.v7.widget.PopupMenu(view.getContext(),view );
            popup.inflate(R.menu.menu_list_overflow);
            popup.setOnMenuItemClickListener(this);
            popup.show();
        }else {
            PopupMenu popup = new PopupMenu(view.getContext(), view);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                popup.inflate(R.menu.menu_list_overflow);
            } else {
                popup.getMenuInflater().inflate(R.menu.menu_list_overflow, popup.getMenu());
            }
            popup.setOnMenuItemClickListener(this);
            popup.show();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        mPresenter.repoClicked(menuItem.getItemId(), mRepo);
        return true;
    }
}