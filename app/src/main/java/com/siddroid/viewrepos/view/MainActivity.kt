package com.siddroid.viewrepos.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.siddroid.viewrepos.R
import com.siddroid.viewrepos.presenter.RepoPresenter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragment = RepositoryListFragment()
        val data = Bundle()
        data.putInt(fragment.LIST_TYPE,RepoPresenter.REPO_LIST)
        fragment.arguments = data
        supportFragmentManager.beginTransaction().replace(R.id.frmContainer, fragment,"repo_list").commit()
    }
}
