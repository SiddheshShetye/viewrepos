package com.siddroid.viewrepos.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.wsmodel.Contributor;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

/**
 * The type Contributor list view holder.
 * @author Siddhesh
 */
public class ContributorListViewHolder extends RecyclerView.ViewHolder {
    private ImageView imvPic;
    private TextView tvName;
    private WeakReference<Context> mContextRef;

    /**
     * Instantiates a new Contributor list view holder.
     *
     * @param itemView the item view
     */
    public ContributorListViewHolder(View itemView) {
        super(itemView);
        mContextRef = new WeakReference<>(itemView.getContext());
        tvName = (TextView) itemView.findViewById(R.id.lblName);
        imvPic = (ImageView) itemView.findViewById(R.id.imvPic);
        ImageButton ibtnMenu = (ImageButton) itemView.findViewById(R.id.ibtnMore);
        ibtnMenu.setVisibility(View.INVISIBLE);
    }

    /**
     * Bind.
     *
     * @param contributor the contributor
     */
    public void bind(Contributor contributor){
        tvName.setText(contributor.getLogin());
        Picasso.with(mContextRef.get()).load(contributor.getAvatar_url()).into(imvPic,new Callback() {
            @Override
            public void onSuccess() {
                Bitmap imageBitmap = ((BitmapDrawable) imvPic.getDrawable()).getBitmap();
                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContextRef.get().getResources(), imageBitmap);
                imageDrawable.setCircular(true);
                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                imvPic.setImageDrawable(imageDrawable);
            }
            @Override
            public void onError() {
                imvPic.setImageResource(R.drawable.ic_launcher_background);
            }
        });
    }
}
