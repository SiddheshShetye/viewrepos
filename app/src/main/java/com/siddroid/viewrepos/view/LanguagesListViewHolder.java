package com.siddroid.viewrepos.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.siddroid.viewrepos.R;

/**
 * The type Languages list view holder.
 * @author Siddhesh
 */
public class LanguagesListViewHolder extends RecyclerView.ViewHolder {

    private TextView tvName;

    /**
     * Instantiates a new Languages list view holder.
     *
     * @param itemView the item view
     */
    public LanguagesListViewHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.lblName);
    }

    /**
     * Bind.
     *
     * @param language the language
     */
    public void bind(String language){
        tvName.setText(language);
    }

}
