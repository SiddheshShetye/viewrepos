package com.siddroid.viewrepos.wsmodel

data class Contributor(val login: String, val avatar_url: String,val contributions: Int);