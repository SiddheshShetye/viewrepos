package com.siddroid.viewrepos.wsmodel

data class Repository(val name: String, val owner: Owner)

data class Owner(val login: String,val avatar_url: String)