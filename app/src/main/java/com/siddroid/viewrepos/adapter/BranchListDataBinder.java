package com.siddroid.viewrepos.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.view.BranchListViewHolder;
import com.siddroid.viewrepos.wsmodel.Branch;

import java.util.List;

/**
 * The type Branch mList data binder.
 * @author Siddhesh
 */
public class BranchListDataBinder implements ViewRepoGenericAdapter.DataBinder<BranchListViewHolder,Branch> {

    private final List<Branch> mList;

    /**
     * Instantiates a new Branch mList data binder.
     *
     * @param mList the List
     */
    public BranchListDataBinder(List<Branch> mList) {
        this.mList = mList;
    }

    @Override
    public BranchListViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new BranchListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language_branch_list,parent,false));
    }

    @Override
    public void bindData(BranchListViewHolder holder, int position, Branch item) {
        holder.bind(item);
    }

    @Override
    public List<Branch> getList() {
        return mList;
    }
}
