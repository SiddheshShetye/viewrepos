package com.siddroid.viewrepos.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.view.LanguagesListViewHolder;

import java.util.List;

/**
 * The type Language mList data binder.
 * @author Siddhesh
 */
public class LanguageListDataBinder implements ViewRepoGenericAdapter.DataBinder<LanguagesListViewHolder,String> {

    private final List<String> mList;

    /**
     * Instantiates a new Language mList data binder.
     *
     * @param mList the List
     */
    public LanguageListDataBinder(List<String> mList) {
        this.mList = mList;
    }

    @Override
    public LanguagesListViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new LanguagesListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language_branch_list,parent,false));
    }

    @Override
    public void bindData(LanguagesListViewHolder holder, int position, String item) {
        holder.bind(item);
    }

    @Override
    public List<String> getList() {
        return mList;
    }
}
