package com.siddroid.viewrepos.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.presenter.RepoPresenter;
import com.siddroid.viewrepos.view.RepoListViewHolder;
import com.siddroid.viewrepos.wsmodel.Repository;

import java.util.List;

/**
 * The type Repo mList data binder.
 * @author Siddhesh
 */
public class RepoListDataBinder implements ViewRepoGenericAdapter.DataBinder<RepoListViewHolder,Repository> {

    private final List<Repository> mList;
    private RepoPresenter mPresenter;

    /**
     * Instantiates a new Repo mList data binder.
     *
     * @param mPresenter the Presenter
     * @param mList      the List
     */
    public RepoListDataBinder(RepoPresenter mPresenter, List<Repository> mList) {
        this.mList = mList;
        this.mPresenter = mPresenter;
    }

    @Override
    public RepoListViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new RepoListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository_contributor_list,parent,false), mPresenter);
    }

    @Override
    public void bindData(RepoListViewHolder holder, int position,Repository repo) {
        holder.bind(repo);
    }

    @Override
    public List<Repository> getList() {
        return mList;
    }
}
