package com.siddroid.viewrepos.adapter;

import android.view.View;

public abstract class ViewHolder {
    public View rootView;

    public ViewHolder(View rootView) {
        this.rootView = rootView;
    }
}
