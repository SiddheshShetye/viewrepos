package com.siddroid.viewrepos.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.view.ContributorListViewHolder;
import com.siddroid.viewrepos.wsmodel.Contributor;

import java.util.List;

/**
 * The type Contributor mList data binder.
 * @author Siddhesh
 */
public class ContributorListDataBinder implements ViewRepoGenericAdapter.DataBinder<ContributorListViewHolder,Contributor> {

    private final List<Contributor> mList;

    /**
     * Instantiates a new Contributor mList data binder.
     *
     * @param mList the List
     */
    public ContributorListDataBinder(List<Contributor> mList) {
        this.mList = mList;
    }

    @Override
    public ContributorListViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new ContributorListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository_contributor_list,parent,false));
    }

    @Override
    public void bindData(ContributorListViewHolder holder, int position, Contributor item) {
        holder.bind(item);
    }

    @Override
    public List<Contributor> getList() {
        return mList;
    }
}
