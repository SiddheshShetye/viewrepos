package com.siddroid.viewrepos.ws;

import android.support.annotation.NonNull;

import com.siddroid.viewrepos.presenter.OnLoadFinishedListener;
import com.siddroid.viewrepos.wsmodel.Branch;
import com.siddroid.viewrepos.wsmodel.Contributor;
import com.siddroid.viewrepos.wsmodel.Repository;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * The type Repo request builder.
 * @author Siddhesh
 */
public class RepoRequestBuilder extends RetroRequestBuilder<RepoRequestBuilder.RepoService> {

    @Override
    public RepoService getService() {
        return getRetro().create(RepoService.class);
    }

    /**
     * The interface Repo service.
     */
    public interface RepoService {
        /**
         * Load all public repositories call.
         *
         * @param since the since
         * @return the call
         */
        @GET("repositories")
        Call<List<Repository>> loadAllPublicRepositories(@Query("since") String since);

        /**
         * Load repo contributors call.
         *
         * @param ownerName the owner name
         * @param repoName  the repo name
         * @param page      the page
         * @return the call
         */
        @GET("repos/{ownerName}/{repoName}/contributors")
        Call<List<Contributor>> loadRepoContributors(@Path("ownerName") String ownerName, @Path("repoName") String repoName, @Query("page") String page);

        /**
         * Load repo languages call.
         *
         * @param ownerName the owner name
         * @param repoName  the repo name
         * @return the call
         */
        @GET("repos/{ownerName}/{repoName}/languages")
        Call<Map<String,String>> loadRepoLanguages(@Path("ownerName") String ownerName, @Path("repoName") String repoName);

        /**
         * Load repo branches call.
         *
         * @param ownerName the owner name
         * @param repoName  the repo name
         * @return the call
         */
        @GET("repos/{ownerName}/{repoName}/branches")
        Call<List<Branch>> loadRepoBranches(@Path("ownerName") String ownerName, @Path("repoName") String repoName);
    }

    /**
     * Load data.
     *
     * @param since    the since
     * @param listener the listener
     */
    public void loadData(String since, final OnLoadFinishedListener<List<Repository>> listener){
        Call<List<Repository>> call = getService().loadAllPublicRepositories(since);
        call.enqueue(new Callback<List<Repository>>() {
            @Override
            public void onResponse(@NonNull Call<List<Repository>> call, @NonNull Response<List<Repository>> response) {
                listener.onResponse(call,response);
            }

            @Override
            public void onFailure(@NonNull Call<List<Repository>> call,@NonNull Throwable t) {
                listener.onFailure(call,t);
            }
        });
    }

    /**
     * Load contributors.
     *
     * @param owner    the owner
     * @param repo     the repo
     * @param page     the page
     * @param listener the listener
     */
    public void loadContributors(String owner, String repo, String page,final OnLoadFinishedListener<List<Contributor>> listener){
        Call<List<Contributor>> call = getService().loadRepoContributors(owner, repo, page);
        call.enqueue(new Callback<List<Contributor>>() {
            @Override
            public void onResponse(@NonNull Call<List<Contributor>> call, @NonNull Response<List<Contributor>> response) {
                listener.onResponse(call,response);
            }

            @Override
            public void onFailure(@NonNull Call<List<Contributor>> call,@NonNull Throwable t) {
                listener.onFailure(call,t);
            }
        });
    }

    /**
     * Load languages.
     *
     * @param owner    the owner
     * @param repo     the repo
     * @param listener the listener
     */
    public void loadLanguages(String owner, String repo,final OnLoadFinishedListener<Map<String,String>> listener){
        Call<Map<String,String>> call = getService().loadRepoLanguages(owner, repo);
        call.enqueue(new Callback<Map<String,String>>() {
            @Override
            public void onResponse(@NonNull Call<Map<String,String>> call, @NonNull Response<Map<String,String>> response) {
                listener.onResponse(call,response);
            }

            @Override
            public void onFailure(@NonNull Call<Map<String,String>> call,@NonNull Throwable t) {
                listener.onFailure(call,t);
            }
        });
    }

    /**
     * Load branches.
     *
     * @param owner    the owner
     * @param repo     the repo
     * @param listener the listener
     */
    public void loadBranches(String owner, String repo,final OnLoadFinishedListener<List<Branch>> listener){
        Call<List<Branch>> call = getService().loadRepoBranches(owner, repo);
        call.enqueue(new Callback<List<Branch>>() {
            @Override
            public void onResponse(@NonNull Call<List<Branch>> call, @NonNull Response<List<Branch>> response) {
                listener.onResponse(call,response);
            }

            @Override
            public void onFailure(@NonNull Call<List<Branch>> call,@NonNull Throwable t) {
                listener.onFailure(call,t);
            }
        });
    }

    /**
     * Cancel all pending request.
     */
    public void cancelAllPendingRequest(){
        cancelAll();
    }
}
