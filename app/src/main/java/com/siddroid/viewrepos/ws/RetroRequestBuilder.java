package com.siddroid.viewrepos.ws;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class builds <code>Retrofit</code> object
 *
 * @param <T> the type parameter
 * @author Siddhesh
 */
public abstract class RetroRequestBuilder<T> {

    private static final String BASE_URL = "https://api.github.com/";
    private static Retrofit sRetroFit = null;
    private OkHttpClient mHttpClient;

    private HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

    /**
     * Gets retro.
     *
     * @return the retro
     */
    Retrofit getRetro() {

        if (sRetroFit == null) {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            mHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(@NotNull Chain chain) throws IOException {
                            Request request = chain.request().newBuilder()
                                    .build();
                            return chain.proceed(request);
                        }
                    })
                    .addInterceptor(loggingInterceptor)
                    .build();

            sRetroFit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(mHttpClient)
                    .build();
        }
        return sRetroFit;
    }

    /**
     * Cancel all.
     */
    protected void cancelAll(){
        if (mHttpClient != null && null == mHttpClient.dispatcher()) {
            mHttpClient.dispatcher().cancelAll();
        }
    }


    /**
     * Gets service.
     *
     * @return the service
     */
    public abstract T getService();
}
