package com.siddroid.viewrepos.presenter;

import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Response;

/**
 * The interface On load finished listener.
 *
 * @param <T> the type parameter for response
 * @author Siddhesh
 */
public interface OnLoadFinishedListener<T> {
    /**
     * On response.
     *
     * @param call     the call
     * @param response the response
     */
    void onResponse(@NonNull Call<T> call, @NonNull Response<T> response);

    /**
     * On failure.
     *
     * @param call the call
     * @param t    the Throwable
     */
    void onFailure(@NonNull Call<T> call,@NonNull Throwable t);
}
