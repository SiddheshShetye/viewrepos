package com.siddroid.viewrepos.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.siddroid.viewrepos.R;
import com.siddroid.viewrepos.presenter.RepoContract.Presenter;
import com.siddroid.viewrepos.ws.RepoRequestBuilder;
import com.siddroid.viewrepos.wsmodel.Branch;
import com.siddroid.viewrepos.wsmodel.Contributor;
import com.siddroid.viewrepos.wsmodel.Repository;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * The type Repo presenter.
 * @author Siddhesh
 */
public class RepoPresenter implements Presenter {

    private RepoContract.View mView;
    /**
     * The constant REPO_LIST.
     */
    public static final int REPO_LIST = 1;
    /**
     * The constant CONTRIBUTOR_LIST.
     */
    public static final int CONTRIBUTOR_LIST = 2;
    /**
     * The constant LANGUAGES_LIST.
     */
    public static final int LANGUAGES_LIST = 3;
    /**
     * The constant BRANCHES_LIST.
     */
    public static final int BRANCHES_LIST = 4;

    private static final String TAG = "RepoPresenter";

    private RepoRequestBuilder mRepoRequestBuilder;
    private String mNextSince;


    /**
     * Instantiates a new Repo presenter.
     *
     * @param mView the mView
     */
    public RepoPresenter(RepoContract.View mView) {
        this.mView = mView;
        mRepoRequestBuilder = new RepoRequestBuilder();
    }

    @Override
    public void loadAllRepositories(final String since) {
        mView.showLoading();
                mRepoRequestBuilder.loadData(since, new OnLoadFinishedListener<List<Repository>>() {

                    @Override
                    public void onResponse(@NonNull Call<List<Repository>> call, @NonNull Response<List<Repository>> response) {
                        if(response.isSuccessful()) {
                            List<Repository> resp = response.body();
                            mNextSince = extractPageIndex(response.headers().get("link"));
                            if (null == mNextSince) {
                                mView.reachedLastPage();
                            }
                            mView.showRepoList(resp);
                        }else{
                            mView.reachedLastPage();
                            mView.showError();
                        }
                        mView.hideLoading();
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<Repository>> call, @NonNull Throwable t) {
                        RepoPresenter.this.onFailure();
                    }
                });
    }

    @Override
    public void loadAllContributors(String owner, String repo, String page) {
        mView.showLoading();
        mRepoRequestBuilder.loadContributors(owner,repo, page, new OnLoadFinishedListener<List<Contributor>>() {
            @Override
            public void onResponse(@NonNull Call<List<Contributor>> call, @NonNull Response<List<Contributor>> response) {
                if (response.isSuccessful()) {
                    List<Contributor> resp = response.body();
                    mNextSince = extractPageIndex(response.headers().get("link"));
                    if (null == mNextSince) {
                        mView.reachedLastPage();
                    }
                    mView.showContributorList(resp);
                }else {
                    mView.showError();
                    mView.reachedLastPage();
                }
                mView.hideLoading();
            }

            @Override
            public void onFailure(@NonNull Call<List<Contributor>> call, @NonNull Throwable t) {
                RepoPresenter.this.onFailure();
            }
        });
    }

    @Override
    public void loadAllLanguages(String owner, String repo, String page) {
        mView.showLoading();
        mRepoRequestBuilder.loadLanguages(owner,repo, new OnLoadFinishedListener<Map<String, String>>() {
            @Override
            public void onResponse(@NonNull Call<Map<String, String>> call, @NonNull Response<Map<String, String>> response) {
                if (response.isSuccessful()) {
                    Map<String, String> resp = response.body();
                    mNextSince = extractPageIndex(response.headers().get("link"));
                    if (null == mNextSince) {
                        mView.reachedLastPage();
                    }
                    mView.showLanguagesList(resp);
                }else {
                    mView.showError();
                    mView.reachedLastPage();
                }
                mView.hideLoading();
            }

            @Override
            public void onFailure(@NonNull Call<Map<String, String>> call, @NonNull Throwable t) {
                RepoPresenter.this.onFailure();
            }
        });
    }

    @Override
    public void loadAllBranches(String owner, String repo,String page) {
        mView.showLoading();
        mRepoRequestBuilder.loadBranches(owner,repo, new OnLoadFinishedListener<List<Branch>>() {
            @Override
            public void onResponse(@NonNull Call<List<Branch>> call, @NonNull Response<List<Branch>> response) {
                if (response.isSuccessful()) {
                    List<Branch> resp = response.body();
                    mNextSince = extractPageIndex(response.headers().get("link"));
                    if (null == mNextSince) {
                        mView.reachedLastPage();
                    }
                    mView.showBranchesList(resp);
                }else {
                    mView.showError();
                    mView.reachedLastPage();
                }
                mView.hideLoading();
            }

            @Override
            public void onFailure(@NonNull Call<List<Branch>> call, @NonNull Throwable t) {
                RepoPresenter.this.onFailure();
            }
        });
    }

    @Override
    public void loadNextRepoPage() {
        if (!TextUtils.isEmpty(mNextSince)){
            loadAllRepositories(mNextSince);
        }
    }

    @Override
    public void loadNextContributorPage(String owner, String repo) {
        if (!TextUtils.isEmpty(mNextSince)){
            loadAllContributors(owner, repo, mNextSince);
        }
    }

    @Override
    public void loadNextLanguagePage(String owner, String repo) {
        if (!TextUtils.isEmpty(mNextSince)){
            loadAllLanguages(owner, repo, mNextSince);
        }
    }

    @Override
    public void loadNextBranchPage(String owner, String repo) {
        if (!TextUtils.isEmpty(mNextSince)){
            loadAllBranches(owner, repo, mNextSince);
        }
    }

    private String extractPageIndex(String link){
        if (link != null && link.contains("next")){
            int startIndex = link.indexOf('=') + 1;
            return link.substring(startIndex,link.indexOf('>'));
        }else {
            return null;
        }
    }

    private void onFailure(){
        mView.showError();
        mView.hideLoading();
        mView.reachedLastPage();
    }

    @Override
    public void repoClicked(int id,Repository repo) {
        switch (id){
                case R.id.menu_contributors:
                mView.navigateToNextScreen(RepoPresenter.CONTRIBUTOR_LIST,repo);
                    break;
                case R.id.menu_languages:
                    mView.navigateToNextScreen(RepoPresenter.LANGUAGES_LIST,repo);
                    break;
                case R.id.menu_branches:
                    mView.navigateToNextScreen(RepoPresenter.BRANCHES_LIST,repo);
                    break;
                default:
                    Log.e(TAG,"wrong menu clicked");
                    break;
            }
        }

    @Override
    public void cancelAllPendingRequest() {
        mRepoRequestBuilder.cancelAllPendingRequest();
    }
}
