package com.siddroid.viewrepos.presenter;

import com.siddroid.viewrepos.wsmodel.Branch;
import com.siddroid.viewrepos.wsmodel.Contributor;
import com.siddroid.viewrepos.wsmodel.Repository;

import java.util.List;
import java.util.Map;

/**
 * The type Repo contract.
 * @author Siddhesh
 */
public class RepoContract {

    /**
     * The interface Presenter.
     */
    public interface Presenter{
        /**
         * Load all repositories.
         *
         * @param since the since
         */
        void loadAllRepositories(String since);

        /**
         * Load all contributors.
         *
         * @param owner the owner
         * @param repo  the repo
         * @param page  the page
         */
        void loadAllContributors(String owner, String repo,String page);

        /**
         * Load all languages.
         *
         * @param owner the owner
         * @param repo  the repo
         * @param page  the page
         */
        void loadAllLanguages(String owner, String repo,String page);

        /**
         * Load all branches.
         *
         * @param owner the owner
         * @param repo  the repo
         * @param page  the page
         */
        void loadAllBranches(String owner, String repo,String page);

        /**
         * Repo clicked.
         *
         * @param id   the id
         * @param repo the repo
         */
        void repoClicked(int id,Repository repo);

        /**
         * Load next repo page.
         */
        void loadNextRepoPage();

        /**
         * Load next contributor page.
         *
         * @param owner the owner
         * @param repo  the repo
         */
        void loadNextContributorPage(String owner, String repo);

        /**
         * Load next language page.
         *
         * @param owner the owner
         * @param repo  the repo
         */
        void loadNextLanguagePage(String owner, String repo);

        /**
         * Load next branch page.
         *
         * @param owner the owner
         * @param repo  the repo
         */
        void loadNextBranchPage(String owner, String repo);

        /**
         * Cancel all pending request.
         */
        void cancelAllPendingRequest();
    }

    /**
     * The interface View.
     */
    public interface View{
        /**
         * Init repo list.
         */
        void initRepoList();

        /**
         * Init contributor list.
         */
        void initContributorList();

        /**
         * Init languages list.
         */
        void initLanguagesList();

        /**
         * Init branches list.
         */
        void initBranchesList();

        /**
         * Show repo list.
         *
         * @param list the list
         */
        void showRepoList(List<Repository> list);

        /**
         * Show contributor list.
         *
         * @param list the list
         */
        void showContributorList(List<Contributor> list);

        /**
         * Show languages list.
         *
         * @param list the list
         */
        void showLanguagesList(Map<String,String> list);

        /**
         * Show branches list.
         *
         * @param list the list
         */
        void showBranchesList(List<Branch> list);

        /**
         * Show error.
         */
        void showError();

        /**
         * Navigate to next screen.
         *
         * @param listToLoad the list to load
         * @param repo       the repo
         */
        void navigateToNextScreen(int listToLoad,Repository repo);

        /**
         * Show loading.
         */
        void showLoading();

        /**
         * Hide loading.
         */
        void hideLoading();

        /**
         * Reached last page.
         */
        void reachedLastPage();
    }
}
